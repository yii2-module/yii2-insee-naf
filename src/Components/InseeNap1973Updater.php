<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-naf library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeNaf\Components;

use InvalidArgumentException;
use Iterator;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNap1973Lv1SectionInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNap1973Lv2DivisionInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNap1973Lv3GroupInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNap1973Lv4ClassInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeNaf\Models\InseeNap1973Lv1Section;
use Yii2Module\Yii2InseeNaf\Models\InseeNap1973Lv2Division;
use Yii2Module\Yii2InseeNaf\Models\InseeNap1973Lv3Group;
use Yii2Module\Yii2InseeNaf\Models\InseeNap1973Lv4Class;

/**
 * InseeNap1973Updater class file.
 * 
 * This class updates all the InseeNap1973 records.
 * 
 * @author Anastaszor
 */
class InseeNap1973Updater extends ObjectUpdater
{
	
	/**
	 * Updates all the nap1973 n1 section records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNap1973Lv1SectionInterface> $sections
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN1Sections(Iterator $sections) : int
	{
		return $this->saveIteratorEachClass(
			$sections,
			InseeNap1973Lv1Section::class,
			function(ApiFrInseeNap1973Lv1SectionInterface $section) : array
			{
				return [
					'insee_nap1973_lv1_section_id' => (string) $section->getIdNap1973Lv1Section(),
				];
			},
			function(ApiFrInseeNap1973Lv1SectionInterface $section) : array
			{
				return [
					'libelle_long' => $section->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the nap1973 n2 division records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNap1973Lv2DivisionInterface> $divisions
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN2Divisions(Iterator $divisions) : int
	{
		return $this->saveIteratorEachClass(
			$divisions,
			InseeNap1973Lv2Division::class,
			function(ApiFrInseeNap1973Lv2DivisionInterface $division) : array
			{
				return [
					'insee_nap1973_lv2_division_id' => (string) $division->getIdNap1973Lv2Division(),
				];
			},
			function(ApiFrInseeNap1973Lv2DivisionInterface $division) : array
			{
				return [
					'insee_nap1973_lv1_section_id' => $division->getIdNap1973Lv1Section(),
					'libelle_long' => $division->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the nap1973 n3 group records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNap1973Lv3GroupInterface> $groups
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN3Groups(Iterator $groups) : int
	{
		return $this->saveIteratorEachClass(
			$groups,
			InseeNap1973Lv3Group::class,
			function(ApiFrInseeNap1973Lv3GroupInterface $group) : array
			{
				return [
					'insee_nap1973_lv3_group_id' => (string) $group->getIdNap1973Lv3Group(),
				];
			},
			function(ApiFrInseeNap1973Lv3GroupInterface $group) : array
			{
				return [
					'insee_nap1973_lv2_division_id' => $group->getIdNap1973Lv2Division(),
					'libelle_long' => $group->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the nap1973 n4 class records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNap1973Lv4ClassInterface> $classes
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN4Classes(Iterator $classes) : int
	{
		return $this->saveIteratorEachClass(
			$classes,
			InseeNap1973Lv4Class::class,
			function(ApiFrInseeNap1973Lv4ClassInterface $class) : array
			{
				return [
					'insee_nap1973_lv4_class_id' => (string) $class->getIdNap1973Lv4Class(),
				];
			},
			function(ApiFrInseeNap1973Lv4ClassInterface $class) : array
			{
				return [
					'insee_nap1973_lv3_group_id' => $class->getIdNap1973Lv3Group(),
					'insee_naf1993_lv5_subclass_id' => $class->getIdNaf1993Lv5Subclass(),
					'libelle_long' => $class->getLibelle(),
				];
			},
		);
	}
	
}
