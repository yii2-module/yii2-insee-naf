<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-naf library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeNaf\Components;

use InvalidArgumentException;
use Iterator;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv1SectionInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv2DivisionInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv3GroupInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv4ClassInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf1993Lv5SubclassInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf1993Lv1Section;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf1993Lv2Division;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf1993Lv3Group;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf1993Lv4Class;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf1993Lv5Subclass;

/**
 * InseeNaf1993Updater class file.
 * 
 * This class updates all the InseeNaf1993 records.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InseeNaf1993Updater extends ObjectUpdater
{
	
	/**
	 * Updates all the naf1993 n1 section records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf1993Lv1SectionInterface> $sections
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN1Sections(Iterator $sections) : int
	{
		return $this->saveIteratorEachClass(
			$sections,
			InseeNaf1993Lv1Section::class,
			function(ApiFrInseeNaf1993Lv1SectionInterface $section) : array
			{
				return [
					'insee_naf1993_lv1_section_id' => (string) $section->getIdNaf1993Lv1Section(),
				];
			},
			function(ApiFrInseeNaf1993Lv1SectionInterface $section) : array
			{
				return [
					'libelle_long' => $section->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the naf1993 n2 division records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf1993Lv2DivisionInterface> $divisions
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN2Divisions(Iterator $divisions) : int
	{
		return $this->saveIteratorEachClass(
			$divisions,
			InseeNaf1993Lv2Division::class,
			function(ApiFrInseeNaf1993Lv2DivisionInterface $division) : array
			{
				return [
					'insee_naf1993_lv2_division_id' => (string) $division->getIdNaf1993Lv2Division(),
				];
			},
			function(ApiFrInseeNaf1993Lv2DivisionInterface $division) : array
			{
				return [
					'insee_naf1993_lv1_section_id' => $division->getIdNaf1993Lv1Section(),
					'libelle_long' => $division->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the naf1993 n3 group records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf1993Lv3GroupInterface> $groups
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN3Groups(Iterator $groups) : int
	{
		return $this->saveIteratorEachClass(
			$groups,
			InseeNaf1993Lv3Group::class,
			function(ApiFrInseeNaf1993Lv3GroupInterface $group) : array
			{
				return [
					'insee_naf1993_lv3_group_id' => (string) $group->getIdNaf1993Lv3Group(),
				];
			},
			function(ApiFrInseeNaf1993Lv3GroupInterface $group) : array
			{
				return [
					'insee_naf1993_lv2_division_id' => $group->getIdNaf1993Lv2Division(),
					'libelle_long' => $group->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the naf1993 n4 class records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf1993Lv4ClassInterface> $classes
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN4Classes(Iterator $classes) : int
	{
		return $this->saveIteratorEachClass(
			$classes,
			InseeNaf1993Lv4Class::class,
			function(ApiFrInseeNaf1993Lv4ClassInterface $class) : array
			{
				return [
					'insee_naf1993_lv4_class_id' => (string) $class->getIdNaf1993Lv4Class(),
				];
			},
			function(ApiFrInseeNaf1993Lv4ClassInterface $class) : array
			{
				return [
					'insee_naf1993_lv3_group_id' => $class->getIdNaf1993Lv3Group(),
					'libelle_long' => $class->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the naf1993 n5 subclass records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf1993Lv5SubclassInterface> $subclasses
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN5Subclasses(Iterator $subclasses) : int
	{
		return $this->saveIteratorEachClass(
			$subclasses,
			InseeNaf1993Lv5Subclass::class,
			function(ApiFrInseeNaf1993Lv5SubclassInterface $subclass) : array
			{
				return [
					'insee_naf1993_lv5_subclass_id' => (string) $subclass->getIdNaf1993Lv5Subclass(),
				];
			},
			function(ApiFrInseeNaf1993Lv5SubclassInterface $subclass) : array
			{
				return [
					'insee_naf1993_lv4_class_id' => $subclass->getIdNaf1993Lv4Class(),
					'insee_naf2003_lv5_subclass_id' => $subclass->getIdNaf2003Lv5Subclass(),
					'libelle_long' => $subclass->getLibelle(),
				];
			},
		);
	}
	
}
