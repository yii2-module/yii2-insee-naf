<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-naf library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeNaf\Components;

use InvalidArgumentException;
use Iterator;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2003Lv1SectionInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2003Lv2DivisionInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2003Lv3GroupInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2003Lv4ClassInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2003Lv5SubclassInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2003Lv1Section;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2003Lv2Division;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2003Lv3Group;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2003Lv4Class;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2003Lv5Subclass;

/**
 * InseeNaf2003Updater class file.
 * 
 * This class updates all the InseeNaf2003 records.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InseeNaf2003Updater extends ObjectUpdater
{
	
	/**
	 * Updates all the naf2003 n1 section records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf2003Lv1SectionInterface> $sections
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN1Sections(Iterator $sections) : int
	{
		return $this->saveIteratorEachClass(
			$sections,
			InseeNaf2003Lv1Section::class,
			function(ApiFrInseeNaf2003Lv1SectionInterface $section) : array
			{
				return [
					'insee_naf2003_lv1_section_id' => (string) $section->getIdNaf2003Lv1Section(),
				];
			},
			function(ApiFrInseeNaf2003Lv1SectionInterface $section) : array
			{
				return [
					'libelle_long' => $section->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the naf2003 n2 division records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf2003Lv2DivisionInterface> $divisions
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN2Divisions(Iterator $divisions) : int
	{
		return $this->saveIteratorEachClass(
			$divisions,
			InseeNaf2003Lv2Division::class,
			function(ApiFrInseeNaf2003Lv2DivisionInterface $division) : array
			{
				return [
					'insee_naf2003_lv2_division_id' => (string) $division->getIdNaf2003Lv2Division(),
				];
			},
			function(ApiFrInseeNaf2003Lv2DivisionInterface $division) : array
			{
				return [
					'insee_naf2003_lv1_section_id' => $division->getIdNaf2003Lv1Section(),
					'libelle_long' => $division->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the naf2003 n3 group records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf2003Lv3GroupInterface> $groups
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN3Groups(Iterator $groups) : int
	{
		return $this->saveIteratorEachClass(
			$groups,
			InseeNaf2003Lv3Group::class,
			function(ApiFrInseeNaf2003Lv3GroupInterface $group) : array
			{
				return [
					'insee_naf2003_lv3_group_id' => (string) $group->getIdNaf2003Lv3Group(),
				];
			},
			function(ApiFrInseeNaf2003Lv3GroupInterface $group) : array
			{
				return [
					'insee_naf2003_lv2_division_id' => $group->getIdNaf2003Lv2Division(),
					'libelle_long' => $group->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the naf2003 n4 class records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf2003Lv4ClassInterface> $classes
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN4Classes(Iterator $classes) : int
	{
		return $this->saveIteratorEachClass(
			$classes,
			InseeNaf2003Lv4Class::class,
			function(ApiFrInseeNaf2003Lv4ClassInterface $class) : array
			{
				return [
					'insee_naf2003_lv4_class_id' => (string) $class->getIdNaf2003Lv4Class(),
				];
			},
			function(ApiFrInseeNaf2003Lv4ClassInterface $class) : array
			{
				return [
					'insee_naf2003_lv3_group_id' => $class->getIdNaf2003Lv3Group(),
					'libelle_long' => $class->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the naf2003 n5 subclass records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf2003Lv5SubclassInterface> $subclasses
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN5Subclasses(Iterator $subclasses) : int
	{
		return $this->saveIteratorEachClass(
			$subclasses,
			InseeNaf2003Lv5Subclass::class,
			function(ApiFrInseeNaf2003Lv5SubclassInterface $subclass) : array
			{
				return [
					'insee_naf2003_lv5_subclass_id' => (string) $subclass->getIdNaf2003Lv5Subclass(),
				];
			},
			function(ApiFrInseeNaf2003Lv5SubclassInterface $subclass) : array
			{
				return [
					'insee_naf2003_lv4_class_id' => $subclass->getIdNaf2003Lv4Class(),
					'insee_naf2008_lv5_subclass_id' => $subclass->getIdNaf2008Lv5Subclass(),
					'libelle_long' => $subclass->getLibelle(),
				];
			},
		);
	}
	
}
