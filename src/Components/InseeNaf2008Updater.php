<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-naf library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeNaf\Components;

use InvalidArgumentException;
use Iterator;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv1SectionInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv2DivisionInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv3GroupInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv4ClassInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv5SubclassInterface;
use PhpExtended\ApiFrInseeNaf\ApiFrInseeNaf2008Lv6ArtisanatInterface;
use RuntimeException;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2008Lv1Section;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2008Lv2Division;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2008Lv3Group;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2008Lv4Class;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2008Lv5Subclass;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2008Lv6Artisanat;

/**
 * InseeNaf2008Updater class file.
 * 
 * This class updates all the InseeNaf2008 records.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InseeNaf2008Updater extends ObjectUpdater
{
	
	/**
	 * Updates all the naf2008 n1 section records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf2008Lv1SectionInterface> $sections
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN1Sections(Iterator $sections) : int
	{
		return $this->saveIteratorEachClass(
			$sections,
			InseeNaf2008Lv1Section::class,
			function(ApiFrInseeNaf2008Lv1SectionInterface $section) : array
			{
				return [
					'insee_naf2008_lv1_section_id' => (string) $section->getIdNaf2008Lv1Section(),
				];
			},
			function(ApiFrInseeNaf2008Lv1SectionInterface $section) : array
			{
				return [
					'libelle_short' => $section->getLibelle40(),
					'libelle_medium' => $section->getLibelle65(),
					'libelle_long' => $section->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the naf2008 n2 division records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf2008Lv2DivisionInterface> $divisions
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN2Divisions(Iterator $divisions) : int
	{
		return $this->saveIteratorEachClass(
			$divisions,
			InseeNaf2008Lv2Division::class,
			function(ApiFrInseeNaf2008Lv2DivisionInterface $division) : array
			{
				return [
					'insee_naf2008_lv2_division_id' => (string) $division->getIdNaf2008Lv2Division(),
				];
			},
			function(ApiFrInseeNaf2008Lv2DivisionInterface $division) : array
			{
				return [
					'insee_naf2008_lv1_section_id' => $division->getIdNaf2008Lv1Section(),
					'libelle_short' => $division->getLibelle40(),
					'libelle_medium' => $division->getLibelle65(),
					'libelle_long' => $division->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the naf2008 n3 group records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf2008Lv3GroupInterface> $groups
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN3Groups(Iterator $groups) : int
	{
		return $this->saveIteratorEachClass(
			$groups,
			InseeNaf2008Lv3Group::class,
			function(ApiFrInseeNaf2008Lv3GroupInterface $group) : array
			{
				return [
					'insee_naf2008_lv3_group_id' => (string) $group->getIdNaf2008Lv3Group(),
				];
			},
			function(ApiFrInseeNaf2008Lv3GroupInterface $group) : array
			{
				return [
					'insee_naf2008_lv2_division_id' => $group->getIdNaf2008Lv2Division(),
					'libelle_short' => $group->getLibelle40(),
					'libelle_medium' => $group->getLibelle65(),
					'libelle_long' => $group->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the naf2008 n4 class records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf2008Lv4ClassInterface> $classes
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN4Classes(Iterator $classes) : int
	{
		return $this->saveIteratorEachClass(
			$classes,
			InseeNaf2008Lv4Class::class,
			function(ApiFrInseeNaf2008Lv4ClassInterface $class) : array
			{
				return [
					'insee_naf2008_lv4_class_id' => (string) $class->getIdNaf2008Lv4Class(),
				];
			},
			function(ApiFrInseeNaf2008Lv4ClassInterface $class) : array
			{
				return [
					'insee_naf2008_lv3_group_id' => $class->getIdNaf2008Lv3Group(),
					'libelle_short' => $class->getLibelle40(),
					'libelle_medium' => $class->getLibelle65(),
					'libelle_long' => $class->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the naf2008 n5 subclass records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf2008Lv5SubclassInterface> $subclasses
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN5Subclasses(Iterator $subclasses) : int
	{
		return $this->saveIteratorEachClass(
			$subclasses,
			InseeNaf2008Lv5Subclass::class,
			function(ApiFrInseeNaf2008Lv5SubclassInterface $subclass) : array
			{
				return [
					'insee_naf2008_lv5_subclass_id' => (string) $subclass->getIdNaf2008Lv5Subclass(),
				];
			},
			function(ApiFrInseeNaf2008Lv5SubclassInterface $subclass) : array
			{
				return [
					'insee_naf2008_lv4_class_id' => $subclass->getIdNaf2008Lv4Class(),
					'libelle_short' => $subclass->getLibelle40(),
					'libelle_medium' => $subclass->getLibelle65(),
					'libelle_long' => $subclass->getLibelle(),
				];
			},
		);
	}
	
	/**
	 * Updates all the naf2008 n6 artisanat records.
	 * 
	 * @param Iterator<integer, ApiFrInseeNaf2008Lv6ArtisanatInterface> $artisanats
	 * @return integer the number of records updated
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function updateN6Artisanat(Iterator $artisanats) : int
	{
		return $this->saveIteratorEachClass(
			$artisanats,
			InseeNaf2008Lv6Artisanat::class,
			function(ApiFrInseeNaf2008Lv6ArtisanatInterface $artisanat) : array
			{
				return [
					'insee_naf2008_lv6_artisanat_id' => (string) $artisanat->getIdNaf2008Lv6Artisanat(),
				];
			},
			function(ApiFrInseeNaf2008Lv6ArtisanatInterface $artisanat) : array
			{
				return [
					'insee_naf2008_lv5_subclass_id' => $artisanat->getIdNaf2008Lv5Subclass(),
					'libelle_long' => $artisanat->getLibelle(),
				];
			},
		);
	}
	
}
