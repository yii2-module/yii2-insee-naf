<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-naf library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeNaf;

use yii\base\Module;
use yii\BaseYii;
use Yii2Extended\Metadata\Bundle;
use Yii2Extended\Metadata\Record;
use Yii2Module\Helper\BootstrappedModule;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf1993Lv1Section;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf1993Lv2Division;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf1993Lv3Group;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf1993Lv4Class;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf1993Lv5Subclass;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2003Lv1Section;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2003Lv2Division;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2003Lv3Group;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2003Lv4Class;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2003Lv5Subclass;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2008Lv1Section;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2008Lv2Division;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2008Lv3Group;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2008Lv4Class;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2008Lv5Subclass;
use Yii2Module\Yii2InseeNaf\Models\InseeNaf2008Lv6Artisanat;
use Yii2Module\Yii2InseeNaf\Models\InseeNap1973Lv1Section;
use Yii2Module\Yii2InseeNaf\Models\InseeNap1973Lv2Division;
use Yii2Module\Yii2InseeNaf\Models\InseeNap1973Lv3Group;
use Yii2Module\Yii2InseeNaf\Models\InseeNap1973Lv4Class;

/**
 * InseeNafModule class file.
 * 
 * This module is to represent the basic geographic information about french
 * activity codes with official denominations, to be able to bild other
 * resources that count on those structures.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class InseeNafModule extends BootstrappedModule
{
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getBootstrapIconName()
	 */
	public function getBootstrapIconName() : string
	{
		return 'list-check';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return BaseYii::t('InseeNafModule.Module', 'Naf');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getEnabledBundles()
	 */
	public function getEnabledBundles() : array
	{
		return [
			'_2008' => new Bundle(BaseYii::t('InseeNafModule.Module', '2008'), [
				'lv1' => (new Record(InseeNaf2008Lv1Section::class, 'lv1', BaseYii::t('InseeNafModule.Module', 'Lv1 Section')))->enableFullAccess(),
				'lv2' => (new Record(InseeNaf2008Lv2Division::class, 'lv2', BaseYii::t('InseeNafModule.Module', 'Lv2 Division')))->enableFullAccess(),
				'lv3' => (new Record(InseeNaf2008Lv3Group::class, 'lv3', BaseYii::t('InseeNafModule.Module', 'Lv3 Group')))->enableFullAccess(),
				'lv4' => (new Record(InseeNaf2008Lv4Class::class, 'lv4', BaseYii::t('InseeNafModule.Module', 'Lv4 Class')))->enableFullAccess(),
				'lv5' => (new Record(InseeNaf2008Lv5Subclass::class, 'lv5', BaseYii::t('InseeNafModule.Module', 'Lv5 Subclass')))->enableFullAccess(),
				'lv6' => (new Record(InseeNaf2008Lv6Artisanat::class, 'lv6', BaseYii::t('InseeNafModule.Module', 'Lv6 Artisanat')))->enableFullAccess(),
			]),
			'_2003' => new Bundle(BaseYii::t('InseeNafModule.Module', '2003'), [
				'lv1' => (new Record(InseeNaf2003Lv1Section::class, 'lv1', BaseYii::t('InseeNafModule.Module', 'Lv1 Section')))->enableFullAccess(),
				'lv2' => (new Record(InseeNaf2003Lv2Division::class, 'lv2', BaseYii::t('InseeNafModule.Module', 'Lv2 Division')))->enableFullAccess(),
				'lv3' => (new Record(InseeNaf2003Lv3Group::class, 'lv3', BaseYii::t('InseeNafModule.Module', 'Lv3 Group')))->enableFullAccess(),
				'lv4' => (new Record(InseeNaf2003Lv4Class::class, 'lv4', BaseYii::t('InseeNafModule.Module', 'Lv4 Class')))->enableFullAccess(),
				'lv5' => (new Record(InseeNaf2003Lv5Subclass::class, 'lv5', BaseYii::t('InseeNafModule.Module', 'Lv5 Subclass')))->enableFullAccess(),
			]),
			'_1993' => new Bundle(BaseYii::t('InseeNafModule.Module', '1993'), [
				'lv1' => (new Record(InseeNaf1993Lv1Section::class, 'lv1', BaseYii::t('InseeNafModule.Module', 'Lv1 Section')))->enableFullAccess(),
				'lv2' => (new Record(InseeNaf1993Lv2Division::class, 'lv2', BaseYii::t('InseeNafModule.Module', 'Lv2 Division')))->enableFullAccess(),
				'lv3' => (new Record(InseeNaf1993Lv3Group::class, 'lv3', BaseYii::t('InseeNafModule.Module', 'Lv3 Group')))->enableFullAccess(),
				'lv4' => (new Record(InseeNaf1993Lv4Class::class, 'lv4', BaseYii::t('InseeNafModule.Module', 'Lv4 Class')))->enableFullAccess(),
				'lv5' => (new Record(InseeNaf1993Lv5Subclass::class, 'lv5', BaseYii::t('InseeNafModule.Module', 'Lv5 Subclass')))->enableFullAccess(),
			]),
			'_1973' => new Bundle(BaseYii::t('InseeNafModule.Module', '1973'), [
				'lv1' => (new Record(InseeNap1973Lv1Section::class, 'lv1', BaseYii::t('InseeNafModule.Module', 'Lv1 Section')))->enableFullAccess(),
				'lv2' => (new Record(InseeNap1973Lv2Division::class, 'lv2', BaseYii::t('InseeNafModule.Module', 'Lv2 Division')))->enableFullAccess(),
				'lv3' => (new Record(InseeNap1973Lv3Group::class, 'lv3', BaseYii::t('InseeNafModule.Module', 'Lv3 Group')))->enableFullAccess(),
				'lv4' => (new Record(InseeNap1973Lv4Class::class, 'lv4', BaseYii::t('InseeNafModule.Module', 'Lv4 Class')))->enableFullAccess(),
			]),
		];
	}
	
}
