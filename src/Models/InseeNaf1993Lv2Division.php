<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-naf library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeNaf\Models;

use yii\BaseYii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Connection;

/**
 * This is the model class for table "insee_naf1993_lv2_division".
 * 
 * @property string $insee_naf1993_lv2_division_id The id of the naf0 division
 * @property string $insee_naf1993_lv1_section_id The id of the related naf0 section
 * @property string $libelle_long The long libelle of this record
 * 
 * @property InseeNaf1993Lv1Section $inseeNaf1993Lv1Section
 * @property InseeNaf1993Lv3Group[] $inseeNaf1993Lv3Groups
 * 
 * /!\ WARNING /!\
 * This class is generated by the gii module, please do not edit manually this
 * file as the changes may be overritten.
 * 
 * @author Anastaszor
 */
class InseeNaf1993Lv2Division extends ActiveRecord
{
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::tableName()
	 */
	public static function tableName() : string
	{
		return 'insee_naf1993_lv2_division';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::getDb()
	 * @return Connection the database connection used by this AR class
	 * @throws \yii\base\InvalidConfigException
	 * @psalm-suppress MoreSpecificReturnType
	 * @psalm-suppress InvalidNullableReturnType
	 */
	public static function getDb() : Connection
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement, NullableReturnStatement */
		return BaseYii::$app->get('db_insee_naf');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::rules()
	 * @return array<integer, array<integer|string, boolean|integer|float|string|array<integer|string, boolean|integer|float|string>>>
	 */
	public function rules() : array
	{
		return [
			[['insee_naf1993_lv2_division_id', 'insee_naf1993_lv1_section_id', 'libelle_long'], 'required'],
			[['insee_naf1993_lv2_division_id'], 'string', 'max' => 2],
			[['insee_naf1993_lv1_section_id'], 'string', 'max' => 1],
			[['libelle_long'], 'string', 'max' => 255],
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::attributeLabels()
	 * @return array<string, string>
	 */
	public function attributeLabels() : array
	{
		return [
			'insee_naf1993_lv2_division_id' => BaseYii::t('InseeNafModule.Models', 'Insee Naf1993 Lv2 Division ID'),
			'insee_naf1993_lv1_section_id' => BaseYii::t('InseeNafModule.Models', 'Insee Naf1993 Lv1 Section ID'),
			'libelle_long' => BaseYii::t('InseeNafModule.Models', 'Libelle Long'),
		];
	}
	
	/**
	 * @return ActiveQuery
	 * @throws \yii\base\UnknownMethodException
	 */
	public function getInseeNaf1993Lv1Section() : ActiveQuery
	{
		return $this->hasOne(InseeNaf1993Lv1Section::class, ['insee_naf1993_lv1_section_id' => 'insee_naf1993_lv1_section_id']);
	}
	
	/**
	 * @return ActiveQuery
	 * @throws \yii\base\UnknownMethodException
	 */
	public function getInseeNaf1993Lv3Groups() : ActiveQuery
	{
		return $this->hasMany(InseeNaf1993Lv3Group::class, ['insee_naf1993_lv2_division_id' => 'insee_naf1993_lv2_division_id']);
	}
	
}
