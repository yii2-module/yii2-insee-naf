<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-insee-naf library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2InseeNaf\Commands;

use PhpExtended\ApiFrInseeNaf\ApiFrInseeNafEndpoint;
use yii\console\ExitCode;
use Yii2Module\Helper\Commands\ExtendedController;
use Yii2Module\Yii2InseeNaf\Components\InseeNaf1993Updater;
use Yii2Module\Yii2InseeNaf\Components\InseeNaf2003Updater;
use Yii2Module\Yii2InseeNaf\Components\InseeNaf2008Updater;
use Yii2Module\Yii2InseeNaf\Components\InseeNap1973Updater;

/**
 * UpdateController class file.
 * 
 * This command updates all parts of the insee naf database.
 * 
 * @author Anastaszor
 */
class UpdateController extends ExtendedController
{
	
	/**
	 * Updates all the classes of records of the NAF.
	 * 
	 * @return integer the error code, 0 if no error
	 */
	public function actionAll() : int
	{
		return $this->actionNaf2008()
			 + $this->actionNaf2003()
			 + $this->actionNaf1993()
			 + $this->actionNap1973();
	}
	
	/**
	 * Updates the nap1973 nomenclature.
	 * 
	 * @return integer the error code, 0 if no error
	 */
	public function actionNap1973() : int
	{
		return $this->runCallable(function() : int
		{
			$endpoint = new ApiFrInseeNafEndpoint();
			$updater = new InseeNap1973Updater();
			$updater->setLogger($this->getLogger());
			$updater->updateN1Sections($endpoint->getNap1973Lv1SectionIterator());
			$updater->updateN2Divisions($endpoint->getNap1973Lv2DivisionIterator());
			$updater->updateN3Groups($endpoint->getNap1973Lv3GroupIterator());
			$updater->updateN4Classes($endpoint->getNap1973Lv4ClassIterator());

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the naf1993 nomenclature.
	 * 
	 * @return integer the error code, 0 if no error
	 */
	public function actionNaf1993() : int
	{
		return $this->runCallable(function() : int
		{
			$endpoint = new ApiFrInseeNafEndpoint();
			$updater = new InseeNaf1993Updater();
			$updater->setLogger($this->getLogger());
			$updater->updateN1Sections($endpoint->getNaf1993Lv1SectionIterator());
			$updater->updateN2Divisions($endpoint->getNaf1993Lv2DivisionIterator());
			$updater->updateN3Groups($endpoint->getNaf1993Lv3GroupIterator());
			$updater->updateN4Classes($endpoint->getNaf1993Lv4ClassIterator());
			$updater->updateN5Subclasses($endpoint->getNaf1993Lv5SubclassIterator());

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the naf2003 nomenclature.
	 * 
	 * @return integer the error code, 0 if no error
	 */
	public function actionNaf2003() : int
	{
		return $this->runCallable(function() : int
		{
			$endpoint = new ApiFrInseeNafEndpoint();
			$updater = new InseeNaf2003Updater();
			$updater->setLogger($this->getLogger());
			$updater->updateN1Sections($endpoint->getNaf2003Lv1SectionIterator());
			$updater->updateN2Divisions($endpoint->getNaf2003Lv2DivisionIterator());
			$updater->updateN3Groups($endpoint->getNaf2003Lv3GroupIterator());
			$updater->updateN4Classes($endpoint->getNaf2003Lv4ClassIterator());
			$updater->updateN5Subclasses($endpoint->getNaf2003Lv5SubclassIterator());

			return ExitCode::OK;
		});
	}
	
	/**
	 * Updates the naf2008 nomenclature.
	 * 
	 * @return integer the error code, 0 if no error
	 */
	public function actionNaf2008() : int
	{
		return $this->runCallable(function() : int
		{
			$endpoint = new ApiFrInseeNafEndpoint();
			$updater = new InseeNaf2008Updater();
			$updater->setLogger($this->getLogger());
			$updater->updateN1Sections($endpoint->getNaf2008Lv1SectionIterator());
			$updater->updateN2Divisions($endpoint->getNaf2008Lv2DivisionIterator());
			$updater->updateN3Groups($endpoint->getNaf2008Lv3GroupIterator());
			$updater->updateN4Classes($endpoint->getNaf2008Lv4ClassIterator());
			$updater->updateN5Subclasses($endpoint->getNaf2008Lv5SubclassIterator());
			$updater->updateN6Artisanat($endpoint->getNaf2008Lv6ArtisanatIterator());

			return ExitCode::OK;
		});
	}
	
}
