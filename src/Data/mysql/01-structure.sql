/**
 * Database structure required by InseeNafModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-naf
 * @license MIT
 */

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `insee_nap1973_lv1_section`;
DROP TABLE IF EXISTS `insee_nap1973_lv2_division`;
DROP TABLE IF EXISTS `insee_nap1973_lv3_group`;
DROP TABLE IF EXISTS `insee_nap1973_lv4_class`;

DROP TABLE IF EXISTS `insee_naf1993_lv1_section`;
DROP TABLE IF EXISTS `insee_naf1993_lv2_division`;
DROP TABLE IF EXISTS `insee_naf1993_lv3_group`;
DROP TABLE IF EXISTS `insee_naf1993_lv4_class`;
DROP TABLE IF EXISTS `insee_naf1993_lv5_subclass`;

DROP TABLE IF EXISTS `insee_naf2003_lv1_section`;
DROP TABLE IF EXISTS `insee_naf2003_lv2_division`;
DROP TABLE IF EXISTS `insee_naf2003_lv3_group`;
DROP TABLE IF EXISTS `insee_naf2003_lv4_class`;
DROP TABLE IF EXISTS `insee_naf2003_lv5_subclass`;

DROP TABLE IF EXISTS `insee_naf2008_lv1_section`;
DROP TABLE IF EXISTS `insee_naf2008_lv2_division`;
DROP TABLE IF EXISTS `insee_naf2008_lv3_group`;
DROP TABLE IF EXISTS `insee_naf2008_lv4_class`;
DROP TABLE IF EXISTS `insee_naf2008_lv5_subclass`;
DROP TABLE IF EXISTS `insee_naf2008_lv6_artisanat`;

SET foreign_key_checks = 1;

CREATE TABLE `insee_nap1973_lv1_section`
(
	`insee_nap1973_lv1_section_id` CHAR(3) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the nap section',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the nap section data';

CREATE TABLE `insee_nap1973_lv2_division`
(
	`insee_nap1973_lv2_division_id` CHAR(3) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the nap division',
	`insee_nap1973_lv1_section_id` CHAR(3) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related nap section',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the nap division data';

CREATE TABLE `insee_nap1973_lv3_group`
(
	`insee_nap1973_lv3_group_id` CHAR(2) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the nap group',
	`insee_nap1973_lv2_division_id` CHAR(3) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related nap division',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the nap group data';

CREATE TABLE `insee_nap1973_lv4_class`
(
	`insee_nap1973_lv4_class_id` CHAR(5) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the nap class',
	`insee_nap1973_lv3_group_id` CHAR(2) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related nap group',
	`insee_naf1993_lv5_subclass_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf0 subclass',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the nap class data';


CREATE TABLE `insee_naf1993_lv1_section`
(
	`insee_naf1993_lv1_section_id` CHAR(1) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf0 section',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf0 section data';

CREATE TABLE `insee_naf1993_lv2_division`
(
	`insee_naf1993_lv2_division_id` CHAR(2) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf0 division',
	`insee_naf1993_lv1_section_id` CHAR(1) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf0 section',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf0 division data';

CREATE TABLE `insee_naf1993_lv3_group`
(
	`insee_naf1993_lv3_group_id` CHAR(2) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf0 group',
	`insee_naf1993_lv2_division_id` CHAR(2) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf0 division',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf0 group data';

CREATE TABLE `insee_naf1993_lv4_class`
(
	`insee_naf1993_lv4_class_id` CHAR(4) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf0 class',
	`insee_naf1993_lv3_group_id` CHAR(2) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf0 group',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf0 class data';

CREATE TABLE `insee_naf1993_lv5_subclass`
(
	`insee_naf1993_lv5_subclass_id` CHAR(5) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf0 subclass',
	`insee_naf1993_lv4_class_id` CHAR(4) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf0 class',
	`insee_naf2003_lv5_subclass_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf1 subclass',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf0 subclass data';


CREATE TABLE `insee_naf2003_lv1_section`
(
	`insee_naf2003_lv1_section_id` CHAR(1) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf1 section',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf1 section data';

CREATE TABLE `insee_naf2003_lv2_division`
(
	`insee_naf2003_lv2_division_id` CHAR(2) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf1 division',
	`insee_naf2003_lv1_section_id` CHAR(1) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf1 section',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf1 division data';

CREATE TABLE `insee_naf2003_lv3_group`
(
	`insee_naf2003_lv3_group_id` CHAR(2) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf1 group',
	`insee_naf2003_lv2_division_id` CHAR(2) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf1 division',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf1 group data';

CREATE TABLE `insee_naf2003_lv4_class`
(
	`insee_naf2003_lv4_class_id` CHAR(4) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf1 class',
	`insee_naf2003_lv3_group_id` CHAR(2) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf1 group',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf1 class data';

CREATE TABLE `insee_naf2003_lv5_subclass`
(
	`insee_naf2003_lv5_subclass_id` CHAR(5) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf1 subclass',
	`insee_naf2003_lv4_class_id` CHAR(4) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf1 class',
	`insee_naf2008_lv5_subclass_id` CHAR(6) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf2 subclass',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf1 subclass data';


CREATE TABLE `insee_naf2008_lv1_section`
(
	`insee_naf2008_lv1_section_id` CHAR(1) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf2 section',
	`libelle_short` VARCHAR(40) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The short libelle under 40 bytes of this record',
	`libelle_medium` VARCHAR(65) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The medium libelle under 65 bytes of this record',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf2 section data';

CREATE TABLE `insee_naf2008_lv2_division`
(
	`insee_naf2008_lv2_division_id` CHAR(2) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf2 division',
	`insee_naf2008_lv1_section_id` CHAR(1) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf2 section',
	`libelle_short` VARCHAR(40) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The short libelle under 40 bytes of this record',
	`libelle_medium` VARCHAR(65) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The medium libelle under 65 bytes of this record',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf2 division data';

CREATE TABLE `insee_naf2008_lv3_group`
(
	`insee_naf2008_lv3_group_id` CHAR(4) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf2 group',
	`insee_naf2008_lv2_division_id` CHAR(2) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf2 division',
	`libelle_short` VARCHAR(40) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The short libelle under 40 bytes of this record',
	`libelle_medium` VARCHAR(65) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The medium libelle under 65 bytes of this record',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf2 group data';

CREATE TABLE `insee_naf2008_lv4_class`
(
	`insee_naf2008_lv4_class_id` CHAR(5) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf2 class',
	`insee_naf2008_lv3_group_id` CHAR(4) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf2 group',
	`libelle_short` VARCHAR(40) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The short libelle under 40 bytes of this record',
	`libelle_medium` VARCHAR(65) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The medium libelle under 65 bytes of this record',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf2 class data';

CREATE TABLE `insee_naf2008_lv5_subclass`
(
	`insee_naf2008_lv5_subclass_id` CHAR(6) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf2 subclass',
	`insee_naf2008_lv4_class_id` CHAR(5) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf2 class',
	`libelle_short` VARCHAR(40) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The short libelle under 40 bytes of this record',
	`libelle_medium` VARCHAR(65) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The medium libelle under 65 bytes of this record',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf2 subclass data';

CREATE TABLE `insee_naf2008_lv6_artisanat`
(
	`insee_naf2008_lv6_artisanat_id` CHAR(8) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the naf2  artisanat',
	`insee_naf2008_lv5_subclass_id` CHAR(6) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related naf2 subclass',
	`libelle_long` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The long libelle of this record'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the naf2 artisanat data';

