/**
 * Database relations required by InseeNafModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-insee-naf
 * @license MIT
 */

ALTER TABLE `insee_nap1973_lv2_division` ADD CONSTRAINT `fk_insee_nap1973_lv2_insee_nap1973_lv1` FOREIGN KEY (`insee_nap1973_lv1_section_id`) REFERENCES `insee_nap1973_lv1_section`(`insee_nap1973_lv1_section_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `insee_nap1973_lv3_group` ADD CONSTRAINT `fk_insee_nap1973_lv3_insee_nap1973_lv2` FOREIGN KEY (`insee_nap1973_lv2_division_id`) REFERENCES `insee_nap1973_lv2_division`(`insee_nap1973_lv2_division_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `insee_nap1973_lv4_class` ADD CONSTRAINT `fk_insee_nap1973_lv4_insee_nap1973_lv3` FOREIGN KEY (`insee_nap1973_lv3_group_id`) REFERENCES `insee_nap1973_lv3_group`(`insee_nap1973_lv3_group_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 
ALTER TABLE `insee_nap1973_lv4_class` ADD CONSTRAINT `fk_insee_nap1973_lv4_insee_naf1993_lv5` FOREIGN KEY (`insee_naf1993_lv5_subclass_id`) REFERENCES `insee_naf1993_lv5_subclass`(`insee_naf1993_lv5_subclass_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `insee_naf1993_lv2_division` ADD CONSTRAINT `fk_insee_naf1993_lv2_insee_naf1993_lv1` FOREIGN KEY (`insee_naf1993_lv1_section_id`) REFERENCES `insee_naf1993_lv1_section`(`insee_naf1993_lv1_section_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `insee_naf1993_lv3_group` ADD CONSTRAINT `fk_insee_naf1993_lv3_insee_naf1993_lv2` FOREIGN KEY (`insee_naf1993_lv2_division_id`) REFERENCES `insee_naf1993_lv2_division`(`insee_naf1993_lv2_division_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `insee_naf1993_lv4_class` ADD CONSTRAINT `fk_insee_naf1993_lv4_insee_naf1993_lv3` FOREIGN KEY (`insee_naf1993_lv3_group_id`) REFERENCES `insee_naf1993_lv3_group`(`insee_naf1993_lv3_group_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `insee_naf1993_lv5_subclass` ADD CONSTRAINT `fk_insee_naf1993_lv5_insee_naf1993_lv4` FOREIGN KEY (`insee_naf1993_lv4_class_id`) REFERENCES `insee_naf1993_lv4_class`(`insee_naf1993_lv4_class_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 
ALTER TABLE `insee_naf1993_lv5_subclass` ADD CONSTRAINT `fk_insee_naf1993_lv5_insee_naf2003_lv5` FOREIGN KEY (`insee_naf2003_lv5_subclass_id`) REFERENCES `insee_naf2003_lv5_subclass`(`insee_naf2003_lv5_subclass_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `insee_naf2003_lv2_division` ADD CONSTRAINT `fk_insee_naf2003_lv2_insee_naf2003_lv1` FOREIGN KEY (`insee_naf2003_lv1_section_id`) REFERENCES `insee_naf2003_lv1_section`(`insee_naf2003_lv1_section_id`) ON DELETE RESTRICT ON UPDATE RESTRICT; 

ALTER TABLE `insee_naf2003_lv3_group` ADD CONSTRAINT `fk_insee_naf2003_lv3_insee_naf2003_lv2` FOREIGN KEY (`insee_naf2003_lv2_division_id`) REFERENCES `insee_naf2003_lv2_division`(`insee_naf2003_lv2_division_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `insee_naf2003_lv4_class` ADD CONSTRAINT `fk_insee_naf2003_lv4_insee_naf2003_lv3` FOREIGN KEY (`insee_naf2003_lv3_group_id`) REFERENCES `insee_naf2003_lv3_group`(`insee_naf2003_lv3_group_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `insee_naf2003_lv5_subclass` ADD CONSTRAINT `fk_insee_naf2003_lv5_insee_naf2003_lv4` FOREIGN KEY (`insee_naf2003_lv4_class_id`) REFERENCES `insee_naf2003_lv4_class`(`insee_naf2003_lv4_class_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 
ALTER TABLE `insee_naf2003_lv5_subclass` ADD CONSTRAINT `fk_insee_naf2003_lv5_insee_naf2008_lv5` FOREIGN KEY (`insee_naf2008_lv5_subclass_id`) REFERENCES `insee_naf2008_lv5_subclass`(`insee_naf2008_lv5_subclass_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `insee_naf2008_lv2_division` ADD CONSTRAINT `fk_insee_naf2008_lv2_insee_naf2008_lv1` FOREIGN KEY (`insee_naf2008_lv1_section_id`) REFERENCES `insee_naf2008_lv1_section`(`insee_naf2008_lv1_section_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `insee_naf2008_lv3_group` ADD CONSTRAINT `fk_insee_naf2008_lv3_insee_naf2008_lv2` FOREIGN KEY (`insee_naf2008_lv2_division_id`) REFERENCES `insee_naf2008_lv2_division`(`insee_naf2008_lv2_division_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `insee_naf2008_lv4_class` ADD CONSTRAINT `fk_insee_naf2008_lv4_insee_naf2008_lv3` FOREIGN KEY (`insee_naf2008_lv3_group_id`) REFERENCES `insee_naf2008_lv3_group`(`insee_naf2008_lv3_group_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `insee_naf2008_lv5_subclass` ADD CONSTRAINT `fk_insee_naf2008_lv5_insee_naf2008_lv4` FOREIGN KEY (`insee_naf2008_lv4_class_id`) REFERENCES `insee_naf2008_lv4_class`(`insee_naf2008_lv4_class_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

ALTER TABLE `insee_naf2008_lv6_artisanat` ADD CONSTRAINT `fk_insee_naf2008_lv6_insee_naf2008_lv5` FOREIGN KEY (`insee_naf2008_lv5_subclass_id`) REFERENCES `insee_naf2008_lv5_subclass`(`insee_naf2008_lv5_subclass_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 

